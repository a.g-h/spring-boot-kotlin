package by.agh.springbootkotlin

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@WebMvcTest
class HttpControllerTests(@Autowired val mockMvc: MockMvc){
    @MockkBean
    private lateinit var userRepository: UserRepository

    @MockkBean
    private lateinit var articleRepository: ArticleRepository

    @Test
    fun `List articles` (){
        var author = User("sartre", "Jean-Poul", "Sartre")
        val article1 = Article("Existentialism Is a Humanism", "is a 1946 work by the philosopher...",
        "First, it has been reproached as an invitation to people to dwell in quietism of despair.", author)
        val article2 = Article("No Exit", "The original title is the French equivalent of the legal term",
                "Three damned souls, Joseph Garcin, Inèz Serrano, and Estelle Rigault, are brought to the same ...", author)
        every { articleRepository.findAllByOrderByAddedAtDesc() } returns
                listOf(article1,article2)
        mockMvc.perform(get("/api/article").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("\$.[0].author.login").value(author.login))
                .andExpect(jsonPath("\$.[0].slug").value(article1.slug))
                .andExpect(jsonPath("\$.[1].author.login").value(author.login))
                .andExpect(jsonPath("\$.[1].slug").value(article2.slug))
    }

    @Test
    fun `List users`() {
        val frantz = User("kkafka", "Frantz", "Kafka")
        val tolstoy = User("lev_t", "Lev", "Tolstoy")
        every { userRepository.findAll() } returns listOf(frantz, tolstoy)
        mockMvc.perform(get("/api/user/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("\$.[0].login").value(frantz.login))
                .andExpect(jsonPath("\$.[1].login").value(tolstoy.login))
    }
}