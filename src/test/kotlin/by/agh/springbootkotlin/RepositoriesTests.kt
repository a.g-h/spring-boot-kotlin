package by.agh.springbootkotlin

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.repository.findByIdOrNull

@DataJpaTest
class RepositoriesTests @Autowired constructor(
        val entityManager: TestEntityManager,
        val userRepository: UserRepository,
        val articleRepository: ArticleRepository
) {
    @Test
    fun `When findByIdOrNull then return Article`(){
        val author = User("ghostwriter", "Fantz", "Kafka")
        entityManager.persist(author)
        val article = Article("Sleepy cat is sleeping!!!",
                "Breaking news from bedroom....", "Meow-meow-meow", author)
        entityManager.persist(article)
        entityManager.flush()
        val found = articleRepository.findByIdOrNull(article.id!!)
        assertThat(found).isEqualTo(article)
    }

    @Test
    fun `When findByLogin then return User`(){
        val author = User("sherlock", "Sherlock", "Holmes")
        entityManager.persist(author)
        entityManager.flush()
        val user = userRepository.findByLogin(author.login)
        assertThat(user).isEqualTo(author)
    }
}