package by.agh.springbootkotlin

import org.springframework.data.repository.CrudRepository

interface ArticleRepository : CrudRepository<Article, Long>{
    // ? - marks as nullable
    fun findBySlug(slug: String): Article?
    fun findAllByOrderByAddedAtDesc(): Iterable<Article>
}

interface UserRepository : CrudRepository<User, Long>{
    fun findByLogin(login: String) : User?
}