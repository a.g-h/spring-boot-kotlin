package by.agh.springbootkotlin

import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BlogConfiguration {
    @Bean
    fun databaseInitializer(userRepository: UserRepository, articleRepository: ArticleRepository) = ApplicationRunner {
        val user = userRepository.save(User("camus", "Albert", "Camus",
        "French existentialist"))
        articleRepository.save(Article(
                title = "The Plague",
                headline = "published in 1947...",
                content = "story from the point of view of  unknown narrator of a plague sweeping the French",
                author = user
        ))
    }

}